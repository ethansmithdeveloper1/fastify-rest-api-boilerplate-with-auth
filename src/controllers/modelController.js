// Import Dependancies
const boom = require('boom');

// Get Data Models
const Model = require('../models/Model.js');

// Get all models
exports.getModels = async (req, reply) => {
  try {
    let query = req.query;
    const models = await Model.find(query);
    return models;
  } catch (err) {
    throw boom.boomify(err);
  };
};

// Get single model by ID
exports.getSingleModel = async (req, reply) => {
  try {
    const id = req.params.id;
    const model = await Model.findById(id);
    return model;
  } catch (err) {
    throw boom.boomify(err);
  };
};

// Add a new model
exports.addModel = async (req, reply) => {
  try {
    const model = new Model(req.body);
    return model.save();
  } catch (err) {
    throw boom.boomify(err);
  };
};

// Update an existing model
exports.updateModel = async (req, reply) => {
  try {
    const id = req.params.id;
    const model = req.body;
    const { ...updateData } = model;
    const update = await Model.findByIdAndUpdate(id, updateData, { new: true });
    return update;
  } catch (err) {
    throw boom.boomify(err);
  };
};

// Delete a model
exports.deleteModel = async (req, reply) => {
  try {
    const id = req.params.id;
    const model = await Model.findByIdAndRemove(id);
    return model;
  } catch (err) {
    throw boom.boomify(err);
  };
};