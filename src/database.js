// Import Mongoose ODM
const mongoose = require('mongoose');

// Connect to DB
mongoose.connect('')
    .then(() => {
        console.log('Sucessfully Connected to MongoDB');
    })
    .catch((err) => {
        console.log(`Error connecting to MongoDB : ${err}`);
    });

module.exports = mongoose;