//Json Web Token Authentication Dependancies
var jwt = require('express-jwt');
var jwks = require('jwks-rsa');

const jwtCheck = jwt({
    secret: jwks.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: ''
  }),
  audience: '',
  issuer: '',
  algorithms: ['RS256']
});

module.exports = jwtCheck;